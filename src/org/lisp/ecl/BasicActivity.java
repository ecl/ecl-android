package org.lisp.ecl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class BasicActivity extends Activity {
    private static String TAG = "BasicActivity";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.swank) {
            Intent intent = new Intent(this, EmbeddedCommonLisp.class);
            intent.setAction("EVAL");
            if (item.isChecked() == false) {
                item.setChecked(true);
                intent.setData(Uri.parse("(start-swank)"));
            }
            else {
                item.setChecked(false);
                intent.setData(Uri.parse("(stop-swank)"));
            }
            startService(intent);
        } else if (item.getItemId() == R.id.eval) {
            evalDialog();
            return true;
        } else if (item.getItemId() == R.id.about) {
            aboutDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void evalDialog () {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter the S-expression to evaluate");

        // Set up the input
        final EditText input = new EditText(this);
        final BasicActivity mThis = this;
        input.setInputType(InputType.TYPE_CLASS_TEXT |
                           InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        builder.setView(input);

        // Set up the buttons
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton("Eval", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(mThis, EmbeddedCommonLisp.class);
                intent.setAction("EVAL");
                intent.setData(Uri.parse(input.getText().toString()));
                startService(intent);
            }
        });

        builder.show();
    }

    private void aboutDialog () {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        TextView text = new TextView(this);
        String content = getResources().getString(R.string.about_content);

        builder.setTitle("About ECL Android");
        text.setText(Html.fromHtml(content));
        text.setAutoLinkMask(Linkify.ALL);
        text.setMovementMethod(LinkMovementMethod.getInstance());

        builder.setView(text);

        // Set up the buttons
        builder.setNegativeButton("Website", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                    myWebLink.setData(Uri.parse("https://common-lisp.net/project/ecl"));
                    startActivity(myWebLink);
                }
            });

        builder.setNeutralButton("Donate", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7B6BP6HF5K2SU"));
                startActivity(myWebLink);
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

        builder.show();
    }
}
